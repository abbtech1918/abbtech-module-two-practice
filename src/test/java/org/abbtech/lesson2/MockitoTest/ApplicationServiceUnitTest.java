package org.abbtech.lesson2.MockitoTest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ApplicationServiceUnitTest {
    @Mock
    private CalculatorServiceImpl calculatorService;
    @InjectMocks
    private  ApplicationService applicationService;
    @Test
    void testSum(){
        when(calculatorService.toplama(anyInt(),anyInt())).thenReturn(32);
        int actual = applicationService.topla(10,22);
        verify(calculatorService,times(1)).toplama(10,22);
        Assertions.assertEquals(12, actual);
    }
}
