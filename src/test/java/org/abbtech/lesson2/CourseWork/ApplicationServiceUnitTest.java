package org.abbtech.lesson2.CourseWork;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ApplicationServiceUnitTest {

    @Mock
    private CalculatorServiceImpl calculatorService;

    @InjectMocks
    private ApplicationService applicationService;

    @Test
    void multiply_success() {
        when(calculatorService.multiply(anyInt(), anyInt())).thenReturn(12);
        int actual = applicationService.multiply(5, 7);
        verify(calculatorService, times(2)).multiply(5, 7);
        Assertions.assertEquals(12, actual);
    }

    @Test
    void subtract_success() {
        when(calculatorService.subtract(anyInt(), anyInt())).thenReturn(7);
        int actual = applicationService.subtract(10, 3);
        verify(calculatorService, times(1)).subtract(10, 3);
        Assertions.assertEquals(7, actual);
    }

    @ParameterizedTest
    @CsvSource(value = {"8,6,2", "8,4,4", "10,4,6", "10,2,8"}, delimiter = ',')
    void subtract_throws(int a, int b, int result) {
        when(calculatorService.subtract(a, b)).thenReturn(result);
        Exception exception = Assertions.assertThrows(ArithmeticException.class, () -> {
            applicationService.subtract(a, b);
        });
        Assertions.assertInstanceOf(ArithmeticException.class, exception);
        verify(calculatorService, times(1)).subtract(a, b);
    }

    @ParameterizedTest
    @CsvSource(value = {"5,7", "6,8", "10,20"}, delimiter = ',')
    void multiply_throws(int a, int b) {
        Exception exception = Assertions.assertThrows(ArithmeticException.class, () -> {
            applicationService.multiply(a, b);
        });
        Assertions.assertInstanceOf(ArithmeticException.class, exception);
    }

}

