package org.abbtech.lesson2.HomeWork_13;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(MockitoExtension.class)
public class UserServiceManagerUnitTest {

    @Mock
    UserRepository userRepository;
    @InjectMocks
    UserServiceManager userServiceManager;
    User user = new User("Arshad","Arshadli","arshad.arshadli",true);

    @Test
    public void isUserActive(){
        // * Fake Data Return
        when(userRepository.findByUsername(user.getUserName())).thenReturn(user);
        // * Actual Data Return
        boolean result = userServiceManager.isUserActive(user.getUserName());
        // Assert
        assertTrue(result);
    }
    @Test
    public void deleteUser(){
        // * Fake Data Return
        when(userRepository.findUserId(12L)).thenReturn(null);
        // * Actual Data Return
        Exception exception = assertThrows(NullPointerException.class,()->{
            userServiceManager.deleteUser(12L);
        });
        // Assert
        Assertions.assertInstanceOf(NullPointerException.class, exception);
    }
    @Test
    public void getUser(){
        // * Fake Data Return
        when(userRepository.findUserId(1L)).thenReturn(user);
        when(userRepository.findUserId(2L)).thenReturn(null);
        // * Actual Data Return
        Exception exception = assertThrows(NullPointerException.class,()->{
            userServiceManager.getUser(2L);
        });
        User result = userServiceManager.getUser(1L);
        // Assert
        Assertions.assertInstanceOf(NullPointerException.class, exception);
        assertEquals(user, result);
    }
}
