package org.abbtech.lesson1.HomeWork;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CalculatorTest {
    Calculator calculator;

    @BeforeEach
    void init() {
        calculator = new Calculator();
    }

    @Test
    void addTest() {
        Double actualResult = calculator.add(10.0, 5.0);
        assertEquals(15.0, actualResult);
    }

    @Test
    void subtractTest() {
        Double actualResult = calculator.subtract(10.0, 5.0);
        assertEquals(5.0, actualResult);
    }

    @Test
    void multiplyTest() {
        Double actualResult = calculator.multiply(10.0, 5.0);
        assertEquals(50.0, actualResult);
    }

    @Test
    void divideTest() {
        Double actualResult = calculator.divide(10.0, 5.0);
        assertEquals(2.0, actualResult);
    }

    @Test
    void subtractIsNegativeWithAssertTrueTest() {
        Double actualResult = calculator.subtract(5.0, 10.0);
        assertTrue(actualResult < 0);
    }

    @Test
    void subtractIsNegativeWithAssertFalseTest() {
        Double actualResult = calculator.subtract(5.0, 10.0);
        assertFalse(actualResult > 0);
    }

    @Test
    void subtractIsNullWithAssertNullTest() {
        Double actualResult = calculator.subtract(null, null);
        assertNull(actualResult);
    }

    @Test
    void subtractIsNotNullWithAssertNotNullTest() {
        Double actualResult = calculator.subtract(1.00, 2.00);
        assertNotNull(actualResult);
    }

    @Test
    void subtractIsArrayEqualsWithAssertArrayEqualsTest() {
        Double[] actualResultArray = new Double[]{calculator.subtract(2.00, 2.00), calculator.subtract(2.00, 2.00), calculator.subtract(2.00, 2.00)};
        Double[] expectedResultArray = new Double[]{0.00, 0.00, 0.00};
        assertArrayEquals(expectedResultArray, actualResultArray);
    }

    @Test
    void subtractIsSameWithAssertSameTest() {
        Calculator calculator1 = new Calculator();
        Calculator calculator2 = calculator1;
        assertSame(calculator1, calculator2);
    }

    @Test
    void subtractIsNotSameWithAssertNotSameTest() {
        Calculator calculator1 = new Calculator();
        Calculator calculator2 = new Calculator();
        assertNotSame(calculator1, calculator2);
    }
    @Test
    void divideByZeroTest() {
        assertThrows(IllegalArgumentException.class, () -> {
            calculator.divide(10.0, 0.0);
        });
    }

}
