package org.abbtech.lesson1.CourseWork;

import org.abbtech.lesson1.CourseWork.Calculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CalculatorTest {
    Calculator calculator;

    @BeforeEach
    void init() {
        calculator = new Calculator();
    }

    @Test
    void sumNumbersTest() {
        int actualResult = calculator.sumNumbers(10, 5);
        Assertions.assertEquals(actualResult, 15);
    }

    @Test
    void  multiplyNumbersTest() {
        int actualResult = Calculator.multiplyNumbers(10, 5);
        Assertions.assertEquals(actualResult, 50);
    }
}
