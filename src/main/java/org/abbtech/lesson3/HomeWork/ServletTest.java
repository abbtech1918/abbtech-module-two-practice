package org.abbtech.lesson3.HomeWork;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ServletTest", urlPatterns = {"/servlet-test-get", "/servlet-test-get-1"})
public class ServletTest extends HttpServlet {
    @Override
    public void init(ServletConfig config) throws ServletException {
        System.out.println("App Initialize Successfully");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        resp.setContentType("application/json");
        writer.write("""
                {
                   "title": "ABB TECH ACADEMY",
                }
                """);
    }
}
