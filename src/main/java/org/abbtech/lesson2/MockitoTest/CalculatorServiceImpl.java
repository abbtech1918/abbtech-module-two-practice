package org.abbtech.lesson2.MockitoTest;

public class CalculatorServiceImpl implements CalculatorService {
    @Override
    public int toplama(int a, int b) {
        return a + b;
    }

    @Override
    public int cixma(int a, int b) {
        return a - b;
    }
}
