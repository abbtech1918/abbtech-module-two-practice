package org.abbtech.lesson2.MockitoTest;

public class ApplicationService {
    private final CalculatorService calculatorService;
    public ApplicationService(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }
    public int topla(int a,int b){
        return calculatorService.toplama(a,b);
    }
    public int cix(int a,int b){
        return calculatorService.cixma(a,b);
    }

}
