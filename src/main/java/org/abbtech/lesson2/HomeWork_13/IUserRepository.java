package org.abbtech.lesson2.HomeWork_13;

public interface IUserRepository {
    User findByUsername(String userName);
    User findUserId(long id);

}
