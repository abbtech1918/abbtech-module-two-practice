package org.abbtech.lesson2.HomeWork_13;

public class User {
    public static long count = 0;
    private long id;
    private String firstName;
    private String lastName;
    private String userName;
    private boolean isActive;

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getUserName() {
        return userName;
    }

    public boolean isActive() {
        return isActive;
    }

    public User(String firstName, String lastName, String userName, boolean isActive) {
        this.id = ++count;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.isActive = isActive;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", userName='" + userName + '\'' +
                ", isActive=" + isActive +
                '}';
    }
}
