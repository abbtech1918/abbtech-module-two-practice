package org.abbtech.lesson2.HomeWork_13;

public class UserServiceManager {
    private final UserRepository userRepository;

    public UserServiceManager(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    public boolean isUserActive(String userName) {
        User user = userRepository.findByUsername(userName);
        return user != null && user.isActive();
    }

    public void deleteUser(Long userId) {
        User user = userRepository.findUserId(userId);
        if (user == null) {
            throw new NullPointerException();
        }
    }

    public User getUser(Long userId) {
        User user = userRepository.findUserId(userId);
        if (user == null) {
            throw new NullPointerException();
        }
        return user;
    }
}
