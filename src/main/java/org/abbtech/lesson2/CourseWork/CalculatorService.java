package org.abbtech.lesson2.CourseWork;

public interface CalculatorService {
    int multiply(int a, int b);

    int subtract(int a, int b);
}
