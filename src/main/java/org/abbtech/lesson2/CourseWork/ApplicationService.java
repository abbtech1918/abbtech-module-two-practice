package org.abbtech.lesson2.CourseWork;

public class ApplicationService {
    private final CalculatorService calculatorService;

    public ApplicationService(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }

    public int multiply(int a, int b) {
//        if (a > 4 && b > 6) {
//            throw new ArithmeticException("a is grt 4, b is grt 6");
//        }
        return calculatorService.multiply(a, b);
    }

    public int subtract(int a, int b) {
        var result = calculatorService.subtract(a, b);
        if (result == 2 || result == 4 || result == 6 || result == 8) {
            throw new ArithmeticException("2, 4, 6 not allowed");
        }
        return result;
    }
}
