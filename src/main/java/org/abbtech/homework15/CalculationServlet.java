package org.abbtech.homework15;

import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "CalculationServlet", urlPatterns = {"/calculator/add", "/calculator/subtract", "/calculator/multiply"})
public class CalculationServlet extends HttpServlet {

    private ApplicationService applicationService;
    private CalculationRepository calculationRepository;

    @Override
    public void init() throws ServletException {
        super.init();
        applicationService = new ApplicationService(new CalculatorServiceImpl());
        calculationRepository = new CalculationRepository();
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        super.service(req, res);
    }

    @Override
    public void destroy() {
        super.destroy();
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int a = Integer.parseInt(req.getParameter("a"));
        int b = Integer.parseInt(req.getParameter("b"));
        int id = Integer.parseInt(req.getParameter("id"));
        PrintWriter writer = resp.getWriter();
        try {
            calculationRepository.updateCalculationResult(id, a, b);
            writer.write("""
                    {
                            "result":""" + "OK" + """
                                
                          }
                    """);
        } catch (ArithmeticException arithmeticException) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            writer.write("""
                    {
                        "exception": "INVALID ARGUMENTS"
                     }                    
                     """);
        }
        resp.setContentType("application/json");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String calculationMethod = req.getHeader("x-calculation-method");
        int a = Integer.parseInt(req.getParameter("a"));
        int b = Integer.parseInt(req.getParameter("b"));
        int result = 0;
        PrintWriter writer = resp.getWriter();

        try {
            if (calculationMethod.equalsIgnoreCase("multiply")) {
                result = applicationService.multiply(a, b);
            }
            calculationRepository.saveCalculationResult(a, b, result, calculationMethod);
            var calculations = calculationRepository.getCalculationResult();
            writer.write("""
                    {
                            "result":""" + calculations + """
                                
                          }
                    """);
        } catch (ArithmeticException arithmeticException) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            writer.write("""
                        {
                        "exception": "INVALID ARGUMENTS"
                        }                
                     """);
        }
        resp.setContentType("application/json");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BufferedReader bufferedReader = req.getReader();
        PrintWriter writer = resp.getWriter();
        int lineLine;
        while ((lineLine = bufferedReader.read()) != -1) {
            char chr = (char) lineLine;
            writer.write(chr);
        }
        writer.close();
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        PrintWriter writer = resp.getWriter();

        try {
            calculationRepository.deleteCalculationResult(id);
            writer.write("""
                    {
                            "result":"ok"
                                
                          }
                    """);
        } catch (Exception exception) {
        }
        resp.setContentType("application/json");
    }
}
