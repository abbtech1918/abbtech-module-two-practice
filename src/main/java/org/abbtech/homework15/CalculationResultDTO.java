package org.abbtech.homework15;

public record CalculationResultDTO(int a, int b, int result, String method) {}
