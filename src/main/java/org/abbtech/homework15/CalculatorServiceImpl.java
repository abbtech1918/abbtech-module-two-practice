package org.abbtech.homework15;

public class CalculatorServiceImpl implements CalculatorService {
    @Override
    public int multiply(int a, int b) {
        return a * b;
    }

    @Override
    public int subtract(int a, int b) {
        return a - b;
    }
}

