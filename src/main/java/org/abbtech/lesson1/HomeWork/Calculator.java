package org.abbtech.lesson1.HomeWork;

import java.util.Optional;

public class Calculator {
    public Double add(Double a, Double b) {
        Optional<Double> optionalA = Optional.ofNullable(a);
        Optional<Double> optionalB = Optional.ofNullable(b);
        if (optionalA.isPresent() && optionalB.isPresent()) {
            return optionalA.get() + optionalB.get();
        } else {
            return null;
        }
    }

    public Double subtract(Double a, Double b) {
        Optional<Double> optionalA = Optional.ofNullable(a);
        Optional<Double> optionalB = Optional.ofNullable(b);
        if (optionalA.isPresent() && optionalB.isPresent()) {
            return optionalA.get() - optionalB.get();
        }
        else {
            return null;
        }
    }

    public Double multiply(Double a, Double b) {
        Optional<Double> optionalA = Optional.ofNullable(a);
        Optional<Double> optionalB = Optional.ofNullable(b);
        if (optionalA.isPresent() && optionalB.isPresent()) {
            return optionalA.get() * optionalB.get();
        } else {
            return null;
        }
    }

    public Double divide(Double a, Double b) {
        Optional<Double> optionalA = Optional.ofNullable(a);
        Optional<Double> optionalB = Optional.ofNullable(b);
        if (optionalA.isPresent() && optionalB.isPresent()) {
            if (optionalB.get() == 0) {
                throw new IllegalArgumentException("Cannot divide by zero");
            }
            return optionalA.get() / optionalB.get();
        } else {
            return null;
        }
    }
}
